# Everforest light soft background
declare-option str bg_dim "e5df65"
declare-option str bg0 "f3ead3"
declare-option str bg1 "eae4ca"
declare-option str bg2 "e3dfc5"
declare-option str bg3 "ddd8be"
declare-option str bg4 "d8d3ba"
declare-option str bg5 "b9c0ab"
declare-option str bg_visual "e1e4bd"
declare-option str bg_red "f4dbd0"
declare-option str bg_green "e5e6c5"
declare-option str bg_blue "e1e7dd"
declare-option str bg_yellow "f1e4c5"

declare-option str fg "5c6a72"
declare-option str red "f85552"
declare-option str orange "f57d26"
declare-option str yellow "dfa000"
declare-option str green "8da101"
declare-option str aqua "35a77c"
declare-option str blue "3a94c5"
declare-option str purple "df69ba"
declare-option str grey0 "a6b0a0"
declare-option str grey1 "939f91"
declare-option str grey2 "829181"

declare-option str cursoralpha "95"

declare-option str statusline1 "93b259"
declare-option str statusline2 "708089"
declare-option str statusline3 "e66868"

source %sh{ printf "%s\n" "$(dirname $kak_source)/everforest-common" }
